package nl.evbox.chargingstation.transformer;

import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeSessionDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ChargeSessionToDtoTransformer implements Function<ChargeSession, ChargeSessionDto> {

    @Override
    public ChargeSessionDto apply(ChargeSession chargeSession) {
        return ChargeSessionDto.builder()
                .id(chargeSession.getId())
                .stationId(chargeSession.getStationId())
                .timestamp(chargeSession.getStartedAt())
                .status(chargeSession.getStatus().toString())
                .build();
    }
}
