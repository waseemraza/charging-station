package nl.evbox.chargingstation.service;

import nl.evbox.chargingstation.exception.InputValidationException;
import nl.evbox.chargingstation.exception.SessionNotFoundException;
import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeSummary;

import java.util.Collection;
import java.util.UUID;

public interface ChargeSessionService {

    /**
     * Get all charging sessions.
     * @return
     */
    Collection<ChargeSession> getAllSession();

    /**
     * Starts a new charging session. It sets the charging status as IN_PROGRESS.
     */
    ChargeSession startSession(final ChargeSession session) throws CloneNotSupportedException, InputValidationException;

    /**
     * It completes a running charging session. It sets the charging status as FINISHED.
     */
    ChargeSession stopSession(final UUID id) throws CloneNotSupportedException, SessionNotFoundException;

    /**
     * It returns overview of the last minute charging session.
     * Overview contains totalCount, in-progress and completed sessions.
     *
     * @return - charging session summary.
     */
    ChargeSummary lastMinuteChargeSummary();
}
