package nl.evbox.chargingstation.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.evbox.chargingstation.exception.InputValidationException;
import nl.evbox.chargingstation.exception.SessionNotFoundException;
import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeStatus;
import nl.evbox.chargingstation.model.ChargeSummary;
import nl.evbox.chargingstation.repository.ChargeSessionRepositoryImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static java.text.MessageFormat.format;

@Slf4j
@Service
@AllArgsConstructor
public class ChargeSessionServiceImpl implements ChargeSessionService {

    private final ChargeSessionRepositoryImpl repository;

    @Override
    public Collection<ChargeSession> getAllSession() {
        return repository.findAllSession();
    }

    @Override
    public ChargeSession startSession(final ChargeSession session) throws CloneNotSupportedException, InputValidationException {
        validateInput(session);
        session.setStatus(ChargeStatus.IN_PROGRESS);
        log.info(format("Charging at stationId = {0} started", session.getStationId()));
        return repository.saveSession(session);
    }

    @Override
    public ChargeSession stopSession(final UUID id) throws CloneNotSupportedException, SessionNotFoundException {
        Objects.requireNonNull(id);
        final Optional<ChargeSession> sessionEntity = repository.findSession(id);
        final ChargeSession chargeSession = sessionEntity.orElseThrow(() -> new SessionNotFoundException(format("Charging Session with id {0} not found.", id)));
        chargeSession.setStatus(ChargeStatus.FINISHED);
        log.info(format("Charging at stationId = {0} completed", chargeSession.getStationId()));
        return repository.saveSession(chargeSession);
    }

    @Override
    public ChargeSummary lastMinuteChargeSummary() {
        final LocalDateTime lastMinute = LocalDateTime.now().minusMinutes(1L);
        return repository.sessionSummary(lastMinute);
    }

    private void validateInput(final ChargeSession session) throws InputValidationException {
        if (session.getStationId() == null || session.getStartedAt() == null)
            throw new InputValidationException(format("stationId and timestamp must not be null. Provided stationId = {0}, timestamp = {1}", session.getStationId(), session.getStartedAt()));
    }
}
