package nl.evbox.chargingstation.exception;

public class InputValidationException extends Exception {

    public InputValidationException() {
    }

    public InputValidationException(final String message) {
        super(message);
    }

}
