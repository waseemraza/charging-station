package nl.evbox.chargingstation.exception;

public class SessionNotFoundException extends Exception {

    public SessionNotFoundException(final String message) {
        super(message);
    }

}
