package nl.evbox.chargingstation.repository;

import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeSummary;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface ChargeSessionRepository {

    /**
     * The method creates or updates a charging session in data stores.
     * If the request object contains UUID then it updates the same otherwise
     * creates a new entry of charging session.
     *
     * Important point to note here is that it returns a cloned object to the caller.
     * If it returns the same instance then any update by the caller will impact the
     * actual saved data in data store.
     *
     * @param chargeSession - Charging session to save or update.
     * @return - Saved or updated charging session.
     * @throws CloneNotSupportedException
     */
    ChargeSession saveSession(final ChargeSession chargeSession) throws CloneNotSupportedException;

    /**
     * It returns a charging session corresponding to session id.
     * If not presents then returns empty Optional.
     *
     * @param id - Id of the charging session.
     * @return - Charging session corresponding to the session id.
     * @throws CloneNotSupportedException
     */
    Optional<ChargeSession> findSession(final UUID id) throws CloneNotSupportedException;

    /**
     * It returns the list of available charging session.
     *
     * @return
     */
    Collection<ChargeSession> findAllSession();

    /**
     * It returns overview of the last minute charging session.
     * Overview contains totalCount, in-progress and completed sessions.
     *
     * @param dateTime - Last minute timestamp.
     * @return
     */
    ChargeSummary sessionSummary(final LocalDateTime dateTime);
}
