package nl.evbox.chargingstation.repository;

import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeStatus;
import nl.evbox.chargingstation.model.ChargeSummary;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Repository
public class ChargeSessionRepositoryImpl implements ChargeSessionRepository {

    // holds all charging session with `UUID` as the key
    // time complexity O(1)
    private ConcurrentHashMap<UUID, ChargeSession> uuidMap;

    // holds all charging session with `start timestamp` as the key
    // time complexity O(logn)
    private SortedMap<LocalDateTime, Map<UUID, ChargeSession>> timestampMap;

    public ChargeSessionRepositoryImpl() {
        this.uuidMap = new ConcurrentHashMap<>();
        this.timestampMap = new ConcurrentSkipListMap<>();
    }

    @Override
    public Collection<ChargeSession> findAllSession() {
        return this.uuidMap.values();
    }

    @Override
    public ChargeSession saveSession(final ChargeSession chargeSession) throws CloneNotSupportedException {

        // protect original data against any modification by the calling method.
        final ChargeSession cloneToSave = chargeSession.clone();

        if (cloneToSave.getId() == null) {
            cloneToSave.setId(UUID.randomUUID());
        }
        this.uuidMap.put(cloneToSave.getId(), cloneToSave);

        timestampMap.computeIfAbsent(cloneToSave.getStartedAt(), (k) ->
                new ConcurrentHashMap<>() {{
                    put(cloneToSave.getId(), cloneToSave);
                }}
        ).put(cloneToSave.getId(), cloneToSave);

        // protect output data against any modification by the calling method.
        return cloneToSave.clone();
    }

    @Override
    public Optional<ChargeSession> findSession(final UUID id) throws CloneNotSupportedException {
        final ChargeSession chargeSession = this.uuidMap.get(id);
        return null == chargeSession ? Optional.empty() : Optional.of(chargeSession.clone());
    }

    @Override
    public ChargeSummary sessionSummary(final LocalDateTime dateTime) {

        // a view of the portion of timestampMap whose keys are greater than or equal to dateTime
        SortedMap<LocalDateTime, Map<UUID, ChargeSession>> lastMinuteChargeSession = timestampMap
                .tailMap(dateTime);

        int totalSessionCount = lastMinuteChargeSession.values().stream()
                .mapToInt(Map::size)
                .sum();

        int stoppedSessionCount = lastMinuteChargeSession.values().stream()
                .mapToInt(map -> (int)map.values().stream()
                        .filter(session -> session.getStatus().equals(ChargeStatus.FINISHED))
                        .count())
                .sum();

        return ChargeSummary.builder()
                .totalCount(totalSessionCount)
                .stoppedCount(stoppedSessionCount)
                .startedCount(totalSessionCount - stoppedSessionCount)
                .build();
    }
}
