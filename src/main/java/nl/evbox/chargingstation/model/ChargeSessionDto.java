package nl.evbox.chargingstation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChargeSessionDto {
    private UUID id;
    private String stationId;
    private String status;
    private LocalDateTime timestamp;
}
