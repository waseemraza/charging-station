package nl.evbox.chargingstation.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class ChargeSession implements Cloneable {

    private UUID id;
    private String stationId;
    @JsonAlias({"timestamp", "startedAt"})
    private LocalDateTime startedAt;
    private ChargeStatus status;

    @Override
    public ChargeSession clone() throws CloneNotSupportedException {
        return (ChargeSession) super.clone();
    }
}

