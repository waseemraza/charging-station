package nl.evbox.chargingstation.model;

public enum ChargeStatus {
    IN_PROGRESS,
    FINISHED
}
