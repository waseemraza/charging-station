package nl.evbox.chargingstation.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChargeSummary {
    private int totalCount;
    private int startedCount;
    private int stoppedCount;
}
