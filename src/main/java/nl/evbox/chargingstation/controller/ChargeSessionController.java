package nl.evbox.chargingstation.controller;

import lombok.AllArgsConstructor;
import nl.evbox.chargingstation.exception.InputValidationException;
import nl.evbox.chargingstation.exception.SessionNotFoundException;
import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeSessionDto;
import nl.evbox.chargingstation.model.ChargeSummary;
import nl.evbox.chargingstation.service.ChargeSessionServiceImpl;
import nl.evbox.chargingstation.transformer.ChargeSessionToDtoTransformer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@AllArgsConstructor
@RequestMapping("/chargingSessions")
public class ChargeSessionController {

    private final ChargeSessionServiceImpl service;
    private final ChargeSessionToDtoTransformer transformer;

    /**
     * Get all charging sessions stored in memory.
     *
     * @return
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ChargeSessionDto>> getAllSession() {
        final List<ChargeSessionDto> sessions = service.getAllSession().stream()
                .map(transformer::apply)
                .collect(Collectors.toList());
        return ResponseEntity.ok(sessions);
    }

    /**
     * Starts a new charging session.
     *
     * @param startSession
     * @return
     * @throws CloneNotSupportedException
     */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChargeSessionDto> startSession(@RequestBody final ChargeSession startSession) throws CloneNotSupportedException, InputValidationException {
        final ChargeSession savedSession = service.startSession(startSession);

        // Don't return status as per requirement
        final ChargeSessionDto dto = ChargeSessionDto.builder()
                .id(savedSession.getId())
                .timestamp(savedSession.getStartedAt())
                .stationId(savedSession.getStationId())
                .build();

        return ResponseEntity.status(CREATED).body(dto);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChargeSessionDto> stopSession(@PathVariable final UUID id) throws CloneNotSupportedException, SessionNotFoundException {
        final ChargeSession stoppedSession = service.stopSession(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(transformer.apply(stoppedSession));
    }

    @GetMapping(value = "/summary", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChargeSummary> chargeSummary() {
        final ChargeSummary chargeSummary = service.lastMinuteChargeSummary();
        return ResponseEntity.ok(chargeSummary);
    }
}
