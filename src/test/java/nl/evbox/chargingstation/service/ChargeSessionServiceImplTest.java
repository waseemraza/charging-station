package nl.evbox.chargingstation.service;

import nl.evbox.chargingstation.exception.InputValidationException;
import nl.evbox.chargingstation.exception.SessionNotFoundException;
import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeStatus;
import nl.evbox.chargingstation.model.ChargeSummary;
import nl.evbox.chargingstation.repository.ChargeSessionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A test class to test all provided services by {@link ChargeSessionServiceImpl}.
 * It doesn't mock any class. It is testing with real data stored in memory.
 */
public class ChargeSessionServiceImplTest {

    private ChargeSessionServiceImpl sessionService;

    @BeforeEach
    void setUp() {
        sessionService = new ChargeSessionServiceImpl(new ChargeSessionRepositoryImpl());
    }

    /**
     * This test confirms
     * 1. starting of a charging session.
     * 2. starting of multiple charging session.
     * 3. stopping of a charging session.
     * 4. start of charging session for 2 stations at the same time.
     * 5. stopping of one charging session when 2 sessions started at the same time.
     *
     * @throws CloneNotSupportedException
     */
    @Test
    void whenUpdateSessionFromSameTime_Finished() throws CloneNotSupportedException, SessionNotFoundException, InputValidationException {
        final LocalDateTime now = LocalDateTime.now();

        final ChargeSession session1Saved = startSession1(now);
        assertThat(session1Saved).isNotNull();
        assertThat(session1Saved.getId().toString()).isEqualTo("62a4768b-73e4-4be2-8417-a9ed8d1e7eea");

        final ChargeSession session2Saved = startSession2(now);
        assertThat(session2Saved).isNotNull();
        assertThat(session2Saved.getId().toString()).isEqualTo("62a4768b-73e4-4be2-8417-a9ed8d1e7ee1");

        final ChargeSession sessionUpdated = sessionService.stopSession(session2Saved.getId());

        assertThat(sessionUpdated.getStatus()).isEqualTo(ChargeStatus.FINISHED);

    }

    @Test
    void whenSaveSessionEmptyObject_SessionNotFoundException() {
        final SessionNotFoundException ex = Assertions.assertThrows(SessionNotFoundException.class, () -> sessionService.stopSession(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7ee1")));
        assertThat(ex).hasMessageContaining("Charging Session with id 62a4768b-73e4-4be2-8417-a9ed8d1e7ee1 not found.");
    }

    @Test
    void whenStopSession_Finished() throws CloneNotSupportedException, SessionNotFoundException, InputValidationException {
        final ChargeSession session = startSession1(LocalDateTime.now());
        final ChargeSession update = sessionService.stopSession(session.getId());
        assertThat(update.getStatus()).isEqualTo(ChargeStatus.FINISHED);
    }

    @Test
    void whenGetSummary_Correct_Count() throws CloneNotSupportedException, SessionNotFoundException, InputValidationException {
        final LocalDateTime sameTimestamp = LocalDateTime.now();
        final ChargeSession session1 = startSession1(sameTimestamp);
        final ChargeSession session2 = startSession2(sameTimestamp);
        final ChargeSession session3 = startSession3(LocalDateTime.now());
        sessionService.stopSession(session2.getId());
        final ChargeSummary summary = sessionService.lastMinuteChargeSummary();
        assertThat(summary.getTotalCount()).isEqualTo(3);
        assertThat(summary.getStoppedCount()).isEqualTo(1);
        assertThat(summary.getStartedCount()).isEqualTo(2);
    }

    private ChargeSession startSession1(final LocalDateTime now) throws CloneNotSupportedException, InputValidationException {
        final ChargeSession session = ChargeSession.builder()
                .id(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7eea"))
                .stationId("stationId1")
                .startedAt(now)
                .build();
        return sessionService.startSession(session);
    }

    private ChargeSession startSession2(final LocalDateTime now) throws CloneNotSupportedException, InputValidationException {
        final ChargeSession session = ChargeSession.builder()
                .id(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7ee1"))
                .stationId("stationId2")
                .startedAt(now)
                .build();
        return sessionService.startSession(session);
    }

    private ChargeSession startSession3(final LocalDateTime now) throws CloneNotSupportedException, InputValidationException {
        final ChargeSession session = ChargeSession.builder()
                .id(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7ee2"))
                .stationId("stationId3")
                .startedAt(now)
                .build();
        return sessionService.startSession(session);
    }
}
