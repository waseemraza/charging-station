package nl.evbox.chargingstation.controller;

import nl.evbox.chargingstation.model.ChargeSession;
import nl.evbox.chargingstation.model.ChargeStatus;
import nl.evbox.chargingstation.service.ChargeSessionServiceImpl;
import nl.evbox.chargingstation.transformer.ChargeSessionToDtoTransformer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This test class is testing rest endpoints. Service layer is mocked here.
 * There is another test class {@link nl.evbox.chargingstation.service.ChargeSessionServiceImplTest}
 * to test all provided services.
 */
@RunWith(SpringRunner.class)
@OverrideAutoConfiguration(enabled = true)
@WebMvcTest({ChargeSessionController.class, ChargeSessionToDtoTransformer.class})
public class ChargeSessionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ChargeSessionServiceImpl service;

    @Test
    public void whenStartChargingSession_Started() throws Exception {
        final ChargeSession createdSession = ChargeSession.builder()
                .id(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7eea"))
                .stationId("ABC-12345")
                .startedAt(LocalDateTime.of(2019, 5, 6, 19, 0, 20))
                .build();
        when(service.startSession(ArgumentMatchers.any(ChargeSession.class))).thenReturn(createdSession);

        this.mvc.perform(post("/chargingSessions")
                .contentType(APPLICATION_JSON_UTF8)
                .content("{\"stationId\":\"ABC-12345\",\"timestamp\":\"2019-05-06T19:00:20\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.stationId", is("ABC-12345")))
                .andExpect(jsonPath("$.id", is("62a4768b-73e4-4be2-8417-a9ed8d1e7eea")));

        verify(service).startSession(ArgumentMatchers.any(ChargeSession.class));
    }

    @Test
    public void whenStopChargingSession_Finished() throws Exception {
        final ChargeSession updatedSession = ChargeSession.builder()
                .id(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7eea"))
                .stationId("ABC-12345")
                .startedAt(LocalDateTime.of(2019, 5, 6, 19, 0, 20))
                .status(ChargeStatus.FINISHED)
                .build();

        when(service.stopSession(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7eea"))).thenReturn(updatedSession);

        this.mvc.perform(put("/chargingSessions/62a4768b-73e4-4be2-8417-a9ed8d1e7eea")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.stationId", is("ABC-12345")))
                .andExpect(jsonPath("$.id", is("62a4768b-73e4-4be2-8417-a9ed8d1e7eea")))
                .andExpect(jsonPath("$.status", is("FINISHED")))
                .andExpect(jsonPath("$.timestamp", is("2019-05-06T19:00:20")));

        verify(service).stopSession(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7eea"));
    }

    @Test
    public void whenGetAllChargingSession_Success() throws Exception {
        final List<ChargeSession> chargeSessions = Collections.singletonList(ChargeSession.builder()
                .id(UUID.fromString("62a4768b-73e4-4be2-8417-a9ed8d1e7eea"))
                .stationId("ABC-12345")
                .startedAt(LocalDateTime.of(2019, 5, 6, 19, 0, 20))
                .status(ChargeStatus.FINISHED)
                .build());

        when(service.getAllSession()).thenReturn(chargeSessions);

        mvc.perform(get("/chargingSessions"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", iterableWithSize(1)))
                .andExpect(jsonPath("$.[0].stationId", equalTo("ABC-12345")));
    }
}
