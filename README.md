# Car Charging Session

## Description
This application represents a store for car charging session entities. It holds all charging session data in memory. It provids REST API to start and stop a charging session. It also provides an API to get summary. Each entity of the store represents unique charging session that can be IN_PROGRESS or FINISHED. Entity has the following fields:
UUID id;
String stationId;
LocalDateTime startedAt;
StatusEnum status;

## Repository 
Charging session data is stored in in-memory data structure. To meet the computational complexity of the problem following classes from java classes are used:- 
1. ConcurrentHashMap: It has O(1) time complexity.
2. ConcurrentSkipListMap: It has O(logn) time complexity. It is a Map, sorted by key (timestamp in this problem).

## Technology
Java 11, Maven 3.6.0, Spring boot 2, Rest API

## Build and Run
Application can run on a machine having java 11 and maven installed. Download the project. Open command prompt and go to the home folder of the project.

**Build the project:** `mvn clean package`

**Start application:** `java -jar .\target\charging-station-1.0.0-SNAPSHOT.jar`

## REST API
### 1. Start New Charging Session:
It starts a new charging session.
 
**URL:** `http://localhost:8080/chargingSessions`
 
**Method:** `POST`

**Request example**

```json
{
"stationId": "ABC-12345",
"timestamp": "2019-05-06T19:00:20.529"
}
``` 
### Success Response
 
**Code:** `201 Completed`

**Response example**
 
 ```json
 {
 "id": "d9bb7458-d5d9-4de7-87f7-7f39edd51d18",
 "stationId": "ABC-12345",
 "timestamp": "2019-05-06T19:00:20.529"
 }
 ``` 
 ### 2. Get All Charging Sessions:
 It retrieves all existing charging sessions.
 
 **URL:** `http://localhost:8080/chargingSessions`
 
 **Method:** `GET`
 ### Success Response
 **Code:** `200 OK`
 
 **Response example**
   
   ```json
   [
       {
       "id": "d9bb7458-d5d9-4de7-87f7-7f39edd51d18",
       "stationId": "ABC-12345",
       "timestamp": "2019-05-06T19:00:20.529",
       "status": "IN_PROGRESS"
       }
   ]
   ```
   
 ### 3. Stop Charging Session:
 It stops a running charging session.
  
 **URL:** `http://localhost:8080/chargingSessions/{id}`
  
 **Method:** `PUT`
 
 ### Success Response
  
 **Code:** `200 OK`
 
 **Response example**
  
  ```json
  {
  "id": "d9bb7458-d5d9-4de7-87f7-7f39edd51d18",
  "stationId": "ABC-12345",
  "timestamp": "2019-05-06T19:00:20.529",
  "status": "FINISHED"
  }
  ```

### 4. Get Summary of Charging Session:
 It stops a running charging session.
  
 **URL:** `http://localhost:8080/chargingSessions/summary`
  
 **Method:** `GET`
 
 ### Success Response
  
 **Code:** `200 OK`
 
 **Response example**
  
  ```json
  {
      "totalCount": 1,
      "startedCount": 1,
      "stoppedCount": 0
  }
  ```
